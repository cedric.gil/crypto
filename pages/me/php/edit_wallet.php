<?php

if (isset($_POST['edit_submit'])) {
    unset($_POST['edit_submit']);
    Engine::Wallet()->setCryptos($_POST, $_GET['wallet'], Engine::Session()->getValue('user'));
    $header = 'Location: /?p=me&sp=wallet&name=' . $_GET['wallet'];
    header($header);
}

?>