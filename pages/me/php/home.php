<?php

$user = Engine::Session()->getValue('user');

if (isset($_POST['wallet_name'])) {
    if (!Engine::Wallet()->newWallet($_POST['wallet_name'], $user)) {
        echo '<script>window.alert("Le porte-feuille existe déjà")</script>';
    }
}

$res = Engine::HtmlHome()->getTop10();

$params = [
    'wallets_list_table' => Engine::HtmlHome()->getWalletList($user),
    'top10_average_percent_24h' => $res['top10_average_percent_24h'],
    'top10_table' => $res['html_top10_table'],
    'metrics_table' => Engine::HtmlHome()->getMetrics(),
    'global_wallet' => Engine::HtmlHome()->getGlobal($user)
];

?>