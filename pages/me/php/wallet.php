<?php

$user = Engine::Session()->getValue('user');
$wallet_name = $_GET['name'];

if (isset($_POST['cryptos_choice'], $_POST['quantity'])) {
    $header = 'Location: /?p=me&sp=add_crypto&wallet=' . $wallet_name . '&user=' . $user . '&crypto=' . $_POST['cryptos_choice'] . '&quantity=' . $_POST['quantity'];
    header($header);
}

$params = [
    'wallet_name' => $wallet_name,
    'global_table' => Engine::HtmlWallet()->getGlobal($user, $wallet_name),
    'wallet_table' => Engine::HtmlWallet()->getWallet($user, $wallet_name),
    'cryptos_datalist' => Engine::HtmlWallet()->getCryptosDatalist()
];

?>