<?php

if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['name']) && isset($_POST['password_repeat'])) {
    switch (Engine::Auth()->register($_POST['name'], $_POST['email'], $_POST['password'], $_POST['password_repeat'])) {
        case 'ok':
            echo '<script type="text/javascript">document.location = "/?p=me"</script>';
            break;
        case 'already_exist':
            echo '<script type="text/javascript">
                window.addEventListener("load", function() {
                    let errorDiv = document.getElementById("error")
                    errorDiv.style.display = "block"
                    errorDiv.innerText = "Le nom d\'utilisateur ou l\'adresse mail existe déjà"
                    
                    let userInput = document.getElementById("name")
                    userInput.setAttribute("value", "' . $_POST['name'] . '")
                    let emailInput = document.getElementById("email")
                    emailInput.setAttribute("value", "' . $_POST['email'] . '")
                })
            </script>';
            break;
        case 'too_lower_password':
            echo '<script type="text/javascript">
                window.addEventListener("load", function() {
                    let errorDiv = document.getElementById("error")
                    errorDiv.style.display = "block"
                    errorDiv.innerText = "Le mot de passe est trop faible\nMinimum 8 charactères, dont une majuscule et un chiffre"
                    
                    let userInput = document.getElementById("name")
                    userInput.setAttribute("value", "' . $_POST['name'] . '")
                    let emailInput = document.getElementById("email")
                    emailInput.setAttribute("value", "' . $_POST['email'] . '")
                })
            </script>';
            break;
        case 'user_same_password':
            echo '<script type="text/javascript">
                window.addEventListener("load", function() {
                    let errorDiv = document.getElementById("error")
                    errorDiv.style.display = "block"
                    errorDiv.innerText = "Le nom d\'utilisateur ne peux pas être similaire au mot de passe"
                    
                    let userInput = document.getElementById("name")
                    userInput.setAttribute("value", "' . $_POST['name'] . '")
                    let emailInput = document.getElementById("email")
                    emailInput.setAttribute("value", "' . $_POST['email'] . '")
                })
            </script>';
            break;
        case 'different_passwords':
            echo '<script type="text/javascript">
                window.addEventListener("load", function() {
                    let errorDiv = document.getElementById("error")
                    errorDiv.style.display = "block"
                    errorDiv.innerText = "Les mots de passent ne correspondent pas"
                    
                    let userInput = document.getElementById("name")
                    userInput.setAttribute("value", "' . $_POST['name'] . '")
                    let emailInput = document.getElementById("email")
                    emailInput.setAttribute("value", "' . $_POST['email'] . '")
                })
            </script>';
            break;
    }
}

?>