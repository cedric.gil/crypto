<?php

/**
 * Class Database
 */
class Database
{
    
    /**
     * Properties
     */
    
    private static $_instance;

    private $pdo;

    public $config;

    private const FETCH_MODE = [
        0 => PDO::FETCH_BOTH,
        1 => PDO::FETCH_ASSOC
    ];



    /**
     * Methods
     */    

    public static function getInstance (Config $config): Database
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new Database($config);
        }
        return self::$_instance;
    }

    private function __construct (Config $config)
    {
        $this->config = $config;
        try {
            $this->pdo = new PDO('mysql:dbname='.$this->config->get('db_name').';host='.$this->config->get('db_host'), $this->config->get('db_user'), $this->config->get('db_pass'));
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Echec de la connexion à la base de données : ' . $e->getMessage();
        }
    }

    public function checkInjection (array $arrayvar)
    {
        foreach ($arrayvar as $var) {
            if (stristr($var, '\'') || stristr($var, '"') || stristr($var, '=') || stristr($var, ';') || stristr($var, '/')) {
                exit(0);
            }
        }
    }

    public function exist (array $array, string $table_name): bool
    {
        $query = "CONTENU CACHE";
        }
        if (sizeof($this->select($query)) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function insert (string $query, array $params)
    {
        $stmt = $this->pdo->prepare($query);
        foreach ($params as $key => &$param) {
            $stmt->bindParam($key + 1, $param);
        }
        $stmt->execute();
    }

    public function select (string $query, int $fetchMode = 0): array
    {
        $stmt = $this->pdo->query($query, self::FETCH_MODE[$fetchMode]);
        return $stmt->fetchAll();
    }

    public function hardQuery (string $query)
    {
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
    }

}

?>