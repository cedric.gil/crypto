<?php

/**
 * Class Auth
 */
class Auth
{

    /**
     * Properties
     */

    private static $_instance;

    private $db;

    private $config;

    private $session;



    /**
     * Methods
     */

    public static function getInstance (Database $db, Config $config, Session $session): Auth
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new Auth($db, $config, $session);
        }
        return self::$_instance;
    }

    private function __construct (Database $db, Config $config, Session $session)
    {
        $this->db = $db;
        $this->config = $config;
        $this->session = $session;
    }

    public function register (string $user, string $email, string $password, string $password_c): string
    {
        $user = strtolower($user);
        $email = strtolower($email);
        $this->db->checkInjection([$user, $email]);
        if ($password !== $password_c) {
            return 'different_passwords';
        } elseif ($user === strtolower($password)) {
            return 'user_same_password';
        } elseif (strlen($password) < 8 || !preg_match('/[A-Z]/', $password) || !preg_match('/[0-9]/', $password)) {
            return 'too_lower_password';
        } elseif ($this->db->exist(['name' => $user, 'email' => $email], 'user')) {
            return 'already_exist';
        } else {
            $password = hash('SHA512', $password . $this->config->get('gds'));
            $this->db->insert("CONTENU CACHE");
            $this->db->hardQuery("CONTENU CACHE");
            $this->session->setValue('user', $user, true);
            return 'ok';
        }
    }

    public function login (string $user, string $password): bool
    {
        $user = strtolower($user);
        $password = hash('SHA512', $password . $this->config->get('gds'));
        $this->db->checkInjection([$user, $password]);
        if (sizeof($this->db->select("CONTENU CACHE")) > 0) {
            $this->session->setValue('user', $user, true);
            return true;
        } else {
            return false;
        }
    }

}

?>