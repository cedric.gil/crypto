<?php

/**
 * Class Twig
 */
class Twig
{

    /**
     * Properties
     */

    private static $_instance;

    private $twig;



    /**
     * Methods
     */

    public static function getInstance (): Twig
    {
        if (is_null(self::$_instance)) {
            self::$_instance = New Twig();
        }
        return self::$_instance;
    }

    private function __construct ()
    {
        $loader = new Twig\Loader\FilesystemLoader('../pages');
        $this->twig = new Twig\Environment($loader, [
            'cache' => false
        ]);
    }

    public function getRender (string $page, ?string $spage = '', array $paramsfromphpfile): string
    {
        $params = $paramsfromphpfile;
        switch ($page) {
            case 'home':
                return $this->twig->render('home.twig', $params);
            case 'register':
                return $this->twig->render('register.twig', $params);
            case 'login':
                return $this->twig->render('login.twig', $params);
            case 'me':
                switch ($spage) {
                    case 'wallet':
                        return $this->twig->render('me/wallet.twig', $params);
                    default:
                        return $this->twig->render('me/home.twig', $params);
                }
            default:
                return $this->twig->render('404.twig');
        }
    }

    public function getPhpPath (string $page, ?string $spage = ''): ?string
    {
        switch ($page) {
            case 'home':
                return '../pages/php/home.php';
            case 'register':
                return '../pages/php/register.php';
            case 'login':
                return '../pages/php/login.php';
            case 'me':
                switch ($spage) {
                    case 'logout':
                        return '../pages/me/php/logout.php';
                    case 'wallet':
                        return '../pages/me/php/wallet.php';
                    case 'edit_wallet':
                        return '../pages/me/php/edit_wallet.php';
                    case 'delete_wallet':
                        return '../pages/me/php/delete_wallet.php';
                    case 'add_crypto':
                        return '../pages/me/php/add_crypto.php';
                    default:
                        return '../pages/me/php/home.php';
                }
            default:
                return null;
        }
    }

}