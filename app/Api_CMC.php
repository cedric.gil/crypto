<?php

class Api_CMC
{

    /**
     * Properties
     */

    private static $_instance;

    private $api_cryptos;

    private $api_metrics;



    /**
     * Methods
     */

    public static function getInstance (): Api_CMC
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new Api_CMC();
        }
        return self::$_instance;
    }

    /**
     * https://coinmarketcap.com/api/documentation/v1
     */
    private function __construct ()
    {
        /**
         * Récupération des données sur les crypto-monnaies
         */
        
        $cmc_api_key = '';
        foreach (explode('/', file_get_contents('../config/cmc_api_keys.txt')) as $key) {
            if (!str_contains($key, '#') && strlen($key) > 0) {
                $cmc_api_key = $key;
            }
        }

        $url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest';
        $parameters = [
            'start' => '1',
            'limit' => '5000'
        ];
        $headers = [
            'Accepts: application/json',
            $cmc_api_key
        ];

        $qs = http_build_query($parameters);
        $request = "{$url}?{$qs}";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $request,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => false
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);
        $this->api_cryptos = $response;

        switch ($this->api_cryptos->status->error_code) {
            case 1010:
            case 1011:
                $this->changeApiKey();
                header('Location: /?p=me');
                exit(0);
            default:
                break;
        }

        /**
         * Récupération des données sur les métriques
         */

        $url = 'https://pro-api.coinmarketcap.com/v1/global-metrics/quotes/latest';
        $headers = [
            'Accepts: application/json',
            $cmc_api_key
        ];

        $qs = http_build_query($parameters);
        $request = "{$url}";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $request,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => false
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);
        $this->api_metrics = $response;

        switch ($this->api_metrics->status->error_code) {
            case 1010:
            case 1011:
                $this->changeApiKey();
                header('Location: /?p=me');
                exit(0);
            default:
                break;
        }

        if (!isset($this->api_cryptos->data) || !isset($this->api_metrics->data)) {
            $this->changeApiKey();
            header('Location: /?p=me');
            exit(0);
        }
    }

    public function getAllCryptos (): array
    {
        $cnt = 0;
        $cryptos = [];
        $total_cap = 0;

        foreach ($this->api_cryptos->data as $crypto) {
            $cnt++;
            $cryptos = array_merge($cryptos, [[
                'symbol' => $crypto->symbol,
                'price' => $crypto->quote->USD->price,
                'percent_24h' => $crypto->quote->USD->percent_change_24h
            ]]);
        }

        return $cryptos;
    }

    public function getMetrics (): array
    {
        $data = $this->api_metrics->data;

        return [
            'total_cap' => $data->quote->USD->total_market_cap,
            'total_cap_percent_change_24h' => $data->quote->USD->total_market_cap_yesterday_percentage_change,
            'altcoin_cap' =>$data->quote->USD->altcoin_market_cap,
            'total_volume_24h' => $data->quote->USD->total_volume_24h,
            'total_volume_percent_change_24h' => $data->quote->USD->total_volume_24h_yesterday_percentage_change,
            'btc_dom' => $data->btc_dominance,
            'btc_dom_percent_change_24h' => $data->btc_dominance_24h_percentage_change,
            'eth_dom' => $data->eth_dominance,
            'eth_dom_percent_change_24h' => $data->eth_dominance_24h_percentage_change,
            'defi_cap' => $data->defi_market_cap,
            'defi_percent_change_24h' => $data->defi_24h_percentage_change,
            'stablecoin_cap' => $data->stablecoin_market_cap,
            'stablecoin_percent_change_24h' => $data->stablecoin_24h_percentage_change,
            'derivatives_volume_24h' => $data->derivatives_volume_24h,
            'derivatives_percent_change_24h' => $data->derivatives_24h_percentage_change
        ];
    }

    public function getCryptoPrices (array $wallet): array
    {
        $api_cryptos = $this->api_cryptos->data;
        $prices = [];
        foreach ($wallet as $key => $value) {
            $i_api = 0;
            while (!isset($crypto_founded)) {
                if ($i_api < 5000) {
                    if ($api_cryptos[$i_api]->symbol == $key) {
                        $prices = array_merge($prices, [$key => [
                            'price' => $api_cryptos[$i_api]->quote->USD->price,
                            'percent_change_24h' => $api_cryptos[$i_api]->quote->USD->percent_change_24h,
                            'volume_24h' => $api_cryptos[$i_api]->quote->USD->volume_24h,
                            'market_cap_dominance' => $api_cryptos[$i_api]->quote->USD->market_cap_dominance
                        ]]);
                        $crypto_founded = true;
                    }
                    $i_api++;
                } else {
                    $crypto_founded = true;
                }
            }
            unset($crypto_founded);
        }
        return $prices;
    }

    public function getRate (): string
    {
        return simplexml_load_file("https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml")->Cube->Cube->Cube['rate']->__toString();
    }

    private function changeApiKey()
    {
        $keys = [];
        foreach (explode('/', file_get_contents('CONTENU CACHE')) as $key) {
            array_push($keys, $key);
        }

        $changed = false;
        foreach ($keys as $key) {
            if (!str_contains($key, '#')) {
                $key = '#' . $key;
            } elseif (!$changed) {
                $key = str_replace('#', '', $key);
                $changed = true;
            }
            if (isset($keys_string)) {
                $keys_string .= '/';
            }
            $keys_string = ($keys_string ?? '') . $key;
        }

        $file = fopen('CONTENU CACHE', 'w');
        fwrite($file, $keys_string ?? '');
        fclose($file);
    }

}

?>