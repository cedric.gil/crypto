<?php

/**
 * Class Session
 */
class Session
{

    /**
     * Properties
     */
    
    static private $_instance;

    private $config;

    

    /**
     * Methods
     */

    static public function getInstance (Config $config): Session
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new Session($config);
        }
        return self::$_instance;
    }

    private function __construct (Config $config)
    {
        $this->config = $config;
    }

    public function setValue (string $key, string $value, ?bool $hash = false)
    {
        global $_SESSION;
        $_SESSION[$key] = $value;
        if ($hash === true) {
            $key = $key . '_hash';
            $value = hash('SHA512', $value . $this->config->get('gds'));
            $_SESSION[$key] = $value;
        }
    }

    public function getValue (string $key): ?string
    {
        global $_SESSION;
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        } else {
            return null;
        }
    }

}

?>