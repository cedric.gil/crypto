<?php

/**
 * Class Engine
 */
class Engine
{

    /**
     * Injection of dependencies
     */



    public static function Api_CMC (): Api_CMC
    {
        return Api_CMC::getInstance();
    }

    public static function Auth (): Auth
    {
        return Auth::getInstance(self::Database(), self::Config(), self::Session());
    }

    public static function Config (): Config
    {
        return Config::getInstance();
    }

    public static function Database (): Database
    {
        return Database::getInstance(self::Config());
    }

    public static function Session (): Session
    {
        return Session::getInstance(self::Config());
    }

    public static function Twig (): Twig
    {
        return Twig::getInstance();
    }
    
    public static function Wallet (): Wallet
    {
        return Wallet::getInstance(self::Database(), self::Session(), self::Config());
    }

    // HTML generators

    public static function HtmlHome (): html\Home
    {
        return html\Home::getInstance(self::Wallet(), self::Api_CMC());
    }

    public static function HtmlWallet (): html\Wallet
    {
        return html\Wallet::getInstance(self::Wallet(), self::Api_CMC(), self::Database());
    }

}

?>