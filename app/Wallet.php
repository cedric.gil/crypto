<?php

/**
 * Class Wallet
 */
class Wallet
{

    /**
     * Properties
     */

    static private $_instance;

    private $db;

    private $session;

    private $config;



    /**
     * Methods
     */

    public static function getInstance (Database $db, Session $session, Config $config): Wallet
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new Wallet($db, $session, $config);
        }
        return self::$_instance;
    }

    private function __construct (Database $db, Session $session, Config $config)
    {
        $this->db = $db;
        $this->session = $session;
        $this->config = $config;
    }

    public function newWallet (string $wallet, string $user): bool
    {
        $this->db->checkInjection([$wallet, $user]);
        $res = $this->db->exist(['wallet_name' => $wallet], 'wallets_' . $user);
        if (!$res) {
            $this->db->insert("INSERT INTO wallets_" . $user . " (wallet_name) VALUE (?)", [$wallet]);
        }
        return !$res;
    }

    public function orderDescWallet (array $wallet, array $prices): array
    {
        $wallet_int_keys = [];
        foreach ($wallet as $key => $value) {
            array_push($wallet_int_keys, [$key, $value]);
        }

        for ($ii = 0; $ii < sizeof($wallet_int_keys); $ii++) {
            if ($wallet_int_keys[$ii][1] > 0) {
                for ($i = $ii+1; $i < sizeof($wallet_int_keys); $i++) {
                    if (($wallet_int_keys[$ii][1] * $prices[$wallet_int_keys[$ii][0]]['price']) < ($wallet_int_keys[$i][1] * $prices[$wallet_int_keys[$i][0]]['price'])) {
                        $temp = $wallet_int_keys[$ii];
                        $wallet_int_keys[$ii] = $wallet_int_keys[$i];
                        $wallet_int_keys[$i] = $temp;
                    }
                }
            }
        }

        $wallet_desc = [];
        foreach ($wallet_int_keys as $value) {
            $wallet_desc = array_merge($wallet_desc, [$value[0] => $value[1]]);
        }

        return $wallet_desc;
    }

    public function getWallet (string $wallet, string $user): array
    {
        $this->db->checkInjection([$wallet, $user]);

        $array = [];
        foreach ($this->db->select("SELECT * FROM wallets_" . $user . " WHERE wallet_name = '" . $wallet . "'", 1) as $res) {
            array_push($array, $res);
        }
        unset($array[0]['wallet_name']);

        foreach ($array[0] as $key => $value) {
            if ($value == 0) {
                unset($array[0][$key]);
            }
        }

        return $array[0];
    }

    public function getWalletsList (string $user): array
    {
        $this->db->checkInjection([$user]);
        $array = [];
        foreach ($this->db->select("SELECT wallet_name FROM wallets_" . $user) as $res) {
            array_push($array, $res['wallet_name']);
        }
        return $array;
    }

    public function deleteWallet (string $wallet, string $user)
    {
        $this->db->checkInjection([$wallet, $user]);
        $this->db->hardQuery("DELETE FROM wallets_" . $user . " WHERE wallet_name = '" . $wallet . "'");
    }

    public function addCrypto (string $crypto, float $quantity, string $wallet, string $user)
    {
        $this->db->checkInjection([$crypto, $quantity, $wallet, $user]);
        if ($this->session->getValue('user_hash') === hash('SHA512', $user . $this->config->get('gds')) && is_numeric($quantity)) {
            $cryptos = [];
            foreach ($this->db->select('SELECT crypto FROM cryptos') as $crypto_) {
                array_push($cryptos, $crypto_['crypto']);
            }
            if (in_array($crypto, $cryptos)) {
                try {
                    $this->db->hardQuery("UPDATE wallets_" . $user . " SET `" . $crypto . "` = " . $quantity . " + `" . $crypto . "` WHERE wallet_name = '" . $wallet . "';");
                } catch (Exception $e) {
                    $this->db->hardQuery("ALTER TABLE `crypto`.`wallets_" . $user . "` 
                    ADD COLUMN `" . $crypto . "` FLOAT NOT NULL DEFAULT 0;
                    UPDATE wallets_" . $user . " SET `" . $crypto . "` = " . $quantity . " + `" . $crypto . "` WHERE wallet_name = '" . $wallet . "';");
                }
            } else {
                echo '<script>window.alert("Vous ne pouvez qu\'ajouter des tokens présents sur CoinMarketCap.")</script>';
            }
        } else {
            $header = 'Location: ?p=me&sp=logout';
            header($header);
        }
    }

    public function setCryptos (array $cryptos, string $wallet, string $user)
    {
        $this->db->checkInjection([$wallet, $user]);
        if ($this->session->getValue('user_hash') === hash('SHA512', $user . $this->config->get('gds'))) {
            foreach ($cryptos as $key => $value) {
                $this->db->checkInjection([$key, $value]);
                $this->db->hardQuery("UPDATE wallets_" . $user . " SET `" . $key . "` = " . $value . " WHERE wallet_name = '" . $wallet . "';");
            }
        } else {
            $header = 'Location: ?p=me&sp=logout';
            header($header);
        }
    }

}
?>  