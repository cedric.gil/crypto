<?php

/**
 * Class MyAutoloader
 */
class MyAutoloader
{

    /**
     * Methods
     */

    public static function register ()
    {
        spl_autoload_register(array(__CLASS__, 'autoloader'));
    }

    public static function autoloader ($class)
    {
        $class = str_replace('\\', '/', $class);
        require 'CONTENU CACHE' . $class . '.php';
    }

}

?>