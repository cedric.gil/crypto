<?php

/**
 * Class Config
 */
class Config
{

    /**
     * Properties
     */

    private static $_instance;

    private array $setting = [];



    /**
     * Methods
     */

    public static function getInstance (): Config
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new Config();
        }
        return self::$_instance;
    }

    private function __construct ()
    {
        if ($_SERVER['PHP_SELF'] === '/index.php') {
            $this->setting = require('CONTENU CACHE');
        }
    }

    public function get (string $key): ?string
    {
        if (!isset($this->setting[$key])) {
            return null;
        }
        return $this->setting[$key];
    }
    
}

?>