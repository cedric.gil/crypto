<?php

namespace html;

class Home
{

    private static $_instance;

    private $wallet;

    private $api_cmc;

    public static function getInstance (\Wallet $wallet, \Api_CMC $api_cmc): Home
    {
        if (is_null(self::$_instance)) {
            self::$_instance = New Home($wallet, $api_cmc);
        }
        return self::$_instance;
    }

    private function __construct (\Wallet $wallet, \Api_CMC $api_cmc)
    {
        $this->wallet = $wallet;
        $this->api_cmc = $api_cmc;
    }

    public function getWalletList (string $user): string
    {
        $rate_usd = $this->api_cmc->getRate();

        $html_wallets_list_table = '<table><tbody>';
        foreach ($this->wallet->getWalletsList($user) as $wallet_name) {
            $wallet = $this->wallet->getWallet($wallet_name, $user);
            $prices = $this->api_cmc->getCryptoPrices($wallet);
            $rate_usd_eur = $this->api_cmc->getRate();

            $global_value = 0;
            $global_value_change = 0;
            if (sizeof($wallet) > 0) {
                foreach ($wallet as $key => $value) {
                    $global_value += $value * $prices[$key]['price'];
                    $global_value_change += ($value * $prices[$key]['price']) - (($value * $prices[$key]['price']) / (($prices[$key]['percent_change_24h'] * 0.01) + 1));
                }
                $global_percent_change = (100 * $global_value_change) / ($global_value - $global_value_change);
            }

            $html_wallets_list_table .= '<tr>
                <td>
                    <div class="div_img_delete_wallet">
                        <a href="/?p=me&sp=delete_wallet&name=' . $wallet_name . '" class="a_img_delete_wallet">
                            <img src="img/cross.png" alt="delete" class="img_delete_wallet" title="Supprimer le porte-feuille">
                        </a>
                    </div>
                    <a href="/?p=me&sp=wallet&name=' . $wallet_name . '" class="a_wallet">
                        ' . $wallet_name . '
                        <div class="wallet_values">';

            if ($global_value_change >= 0) {
                $color = 'green';
                $sign = '+';
            } else {
                $color = 'red';
                $sign = '-';
            }
            if (abs($global_value_change) < 1000) {
                $html_wallets_list_table .= '<span class="rate_usd" style="color: ' . $color . ';">' . $sign . '$' . str_replace('.', ',', round(abs($global_value_change), 2)) . '</span>
                <span class="rate_eur" style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round(abs($global_value_change / $rate_usd_eur), 2)) . '€</span>';
            } else {
                $html_wallets_list_table .= '<span style="color: ' . $color . ';">' . $sign . '$' . str_replace('.', ',', round(abs($global_value_change))) . '</span>';
            }

            $html_wallets_list_table .= '&emsp;';

            if ($global_value < 1000) {
                $html_wallets_list_table .= '<span class="rate_usd">$' . str_replace('.', ',', round($global_value, 2)) . '</span>
                <span class="rate_eur">' . str_replace('.', ',', round($global_value / $rate_usd_eur, 2)) . '€</span>';
            } else {
                $html_wallets_list_table .= '<span class="rate_usd">$' . round($global_value) . '</span>
                <span class="rate_eur">' . round($global_value / $rate_usd_eur) . '€</span>';
            }
            $html_wallets_list_table .= '</div>
                    </a>
                </td>
            </tr>';
        }
        $html_wallets_list_table .= '</tbody></table>';

        return $html_wallets_list_table;
    }

    public function getGlobal (string $user): string
    {
        foreach ($this->wallet->getWalletsList($user) as $wallet_name) {
            $wallet = $this->wallet->getWallet($wallet_name, $user);
            $prices = $this->api_cmc->getCryptoPrices($wallet);
            $wallets_prices = array_merge($wallets_prices ?? [], [$wallet_name => [$wallet, $prices]]);
        }
        $rate_usd_eur = $this->api_cmc->getRate();

        $global_value = 0;
        $global_value_change = 0;
        $global_percent_change = 0;
        if (isset($wallets_prices)) {
            foreach ($wallets_prices as $wallet_name => $wallet_price) {
                if (sizeof($wallet_price[0]) > 0) {
                    $global_value_wallet = 0;
                    $global_value_change_wallet = 0;
                    foreach ($wallet_price[0] as $key => $value) {
                        $global_value_wallet += $value * $wallet_price[1][$key]['price'];
                        $global_value_change_wallet += ($value * $wallet_price[1][$key]['price']) - (($value * $wallet_price[1][$key]['price']) / (($wallet_price[1][$key]['percent_change_24h'] * 0.01) + 1));
                    }
                    $global_percent_change_wallet = (100 * $global_value_change_wallet) / ($global_value_wallet - $global_value_change_wallet);
                    $all_wallets_values = array_merge($all_wallets_values ?? [], [$wallet_name => [
                        'global_value' => $global_value_wallet,
                        'global_value_change' => $global_value_change_wallet,
                        'global_percent_change' => $global_percent_change_wallet
                    ]]);

                }
            }
            if (isset($all_wallets_values)) {
                $global_value = 0;
                $global_value_change = 0;
                foreach ($all_wallets_values as $wallet_values) {
                    $global_value += $wallet_values['global_value'];
                    $global_value_change += $wallet_values['global_value_change'];
                }
                $global_percent_change = (100 * $global_value_change) / ($global_value - $global_value_change);
            }
        }

        $html = '<table>
            <tr>
                <td>Valeur<br>';

        if ($global_percent_change >= 0) {
            $color = 'green';
            $sign = '+';
        } else {
            $color = 'red';
            $sign = '';
        }
        $html .= '<span style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round($global_percent_change, 2)) . '%</span>&emsp;';

        if ($global_value_change >= 0) {
            $color = 'green';
            $sign = '+';
        } else {
            $color = 'red';
            $sign = '-';
        }
        if (abs($global_value_change) < 1000) {
            $html .= '<span class="rate_usd" style="color: ' . $color . ';">' . $sign . '$' . str_replace('.', ',', round(abs($global_value_change), 2)) . '</span>
                <span class="rate_eur" style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round(abs($global_value_change / $rate_usd_eur), 2)) . '€</span><br>';
        } else {
            $html .= '<span style="color: ' . $color . ';">' . $sign . '$' . str_replace('.', ',', round(abs($global_value_change))) . '</span><br>';
        }

        if ($global_value < 1000) {
            $html .= '<span class="rate_usd">$' . str_replace('.', ',', round($global_value, 2)) . '</span>
                <span class="rate_eur">' . str_replace('.', ',', round($global_value / $rate_usd_eur, 2)) . '€</span>';
        } else {
            $html .= '<span class="rate_usd">$' . round($global_value) . '</span>
                <span class="rate_eur">' . round($global_value / $rate_usd_eur) . '€</span>';
        }

        $html .= '</td>
            </tr>
        </table>';

        return $html;
    }

    public function getTop10 (): array
    {
        $html_top10_table = '<table><tbody>';
        $cryptos = $this->api_cmc->getAllCryptos();
        $top = 10;
        $cnt = 0;
        $top10_average_percent_24h = 0;
        $rate_usd_eur = $this->api_cmc->getRate();

        for ($i = 0; $i < $top; $i++) {
            if (!str_contains($cryptos[$i]['symbol'], 'USD') && !str_contains($cryptos[$i]['symbol'], 'DAI')) {
                $cnt++;
                $top10_average_percent_24h += $cryptos[$i]['percent_24h'];
                if ($cryptos[$i]['percent_24h'] >= 0) {
                    $color = 'green';
                    $sign = '+';
                } else {
                    $color = 'red';
                    $sign = '';
                }
                $html_top10_table .= '<tr>
                    <td>' . $cnt . '.</td>
                    <td>' . $cryptos[$i]['symbol'] . '</td>
                    <td style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round($cryptos[$i]['percent_24h'], 2)) . '%</td>';
                if ($cryptos[$i]['price'] >= 1000) {
                    $html_top10_table .= '<td class="rate_usd">$' . str_replace('.', ',', round($cryptos[$i]['price'])) . '</td>
                        <td class="rate_eur">' . str_replace('.', ',', round($cryptos[$i]['price'] / $rate_usd_eur)) . '€</td>';
                } elseif ($cryptos[$i]['price'] >= 1) {
                    $html_top10_table .= '<td class="rate_usd">$' . str_replace('.', ',', round($cryptos[$i]['price'], 2)) . '</td>
                        <td class="rate_eur">' . str_replace('.', ',', round($cryptos[$i]['price'] / $rate_usd_eur, 2)) . '€</td>';
                } elseif ($cryptos[$i]['price'] >= 0.0001) {
                    $html_top10_table .= '<td class="rate_usd">$' . str_replace('.', ',', round($cryptos[$i]['price'], 4)) . '</td>
                        <td class="rate_eur">' . str_replace('.', ',', round($cryptos[$i]['price'] / $rate_usd_eur, 4)) . '€</td>';
                } else {
                    $html_top10_table .= '<td class="rate_usd"><$0,0001</td>
                        <td class="rate_eur"><0,0001€</td>';
                }
                $html_top10_table .= '</tr>';
            } else {
                $top++;
            }
        }

        $top10_average_percent_24h /= 10;
        $top10_average_percent_24h = round($top10_average_percent_24h, 2);
        if ($top10_average_percent_24h >= 0) {
            $color = 'green';
            $sign = '+';
        } else {
            $color = 'red';
            $sign = '';
        }
        
        $top10_average_percent_24h = '<span style="color:' . $color . ';">' . $sign . str_replace('.', ',', $top10_average_percent_24h) . '%</span>';
        $html_top10_table .= '</tbody></table>';

        return [
            'top10_average_percent_24h' => $top10_average_percent_24h,
            'html_top10_table' => $html_top10_table
        ];
    }

    public function getMetrics (): string
    {
        $metrics = $this->api_cmc->getMetrics();
        $rate_usd_eur = $this->api_cmc->getRate();

        $html_metrics_table = '<table><tbody>';

        if ($metrics['total_cap_percent_change_24h'] >= 0) {
            $color = 'green';
            $sign = '+';
        } else {
            $color = 'red';
            $sign = '';
        }
        $html_metrics_table .= '<tr>
            <td>
                Capitalisation totale<br>
                <span style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round($metrics['total_cap_percent_change_24h'], 2)) . '%</span><br>
                <span class="rate_usd">$' . str_replace('.', ',', round($metrics['total_cap'] * 0.000000001, 2)) . ' Mrds</span>
                <span class="rate_eur">' . str_replace('.', ',', round(($metrics['total_cap'] / $rate_usd_eur) * 0.000000001, 2)) . '€ Mrds</span>
            </td>';
        if ($metrics['total_cap_percent_change_24h'] >= 0) {
            $color = 'green';
            $sign = '+';
        } else {
            $color = 'red';
            $sign = '';
        }
        $html_metrics_table .= '<td>
                Capitalisation altcoin<br>
                <span class="rate_usd">$' . str_replace('.', ',', round($metrics['altcoin_cap'] * 0.000000001, 2)) . ' Mrds</span>
                <span class="rate_eur">' . str_replace('.', ',', round(($metrics['altcoin_cap'] / $rate_usd_eur) * 0.000000001, 2)) . '€ Mrds</span>
            </td>
        </tr>';

        if ($metrics['defi_percent_change_24h'] >= 0) {
            $color = 'green';
            $sign = '+';
        } else {
            $color = 'red';
            $sign = '';
        }
        $html_metrics_table .= '<tr>
            <td>
                Capitalisation DeFi<br>
                <span style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round($metrics['defi_percent_change_24h'], 2)) . '%</span><br>
                <span class="rate_usd">$' . str_replace('.', ',', round($metrics['defi_cap'] * 0.000000001, 2)) . ' Mrds</span>
                <span class="rate_eur">' . str_replace('.', ',', round(($metrics['defi_cap'] / $rate_usd_eur) * 0.000000001, 2)) . '€ Mrds</span>
            </td>';
        if ($metrics['stablecoin_percent_change_24h'] >= 0) {
            $color = 'green';
            $sign = '+';
        } else {
            $color = 'red';
            $sign = '';
        }
        $html_metrics_table .= '<td>
                Capitalisation stablecoin<br>
                <span style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round($metrics['stablecoin_percent_change_24h'], 2)) . '%</span><br>
                <span class="rate_usd">$' . str_replace('.', ',', round($metrics['stablecoin_cap'] * 0.000000001, 2)) . ' Mrds</span>
                <span class="rate_eur">' . str_replace('.', ',', round(($metrics['stablecoin_cap'] / $rate_usd_eur) * 0.000000001, 2)) . '€ Mrds</span>
            </td>
        </tr>';

        if ($metrics['btc_dom_percent_change_24h'] >= 0) {
            $color = 'green';
            $sign = '+';
        } else {
            $color = 'red';
            $sign = '';
        }
        $html_metrics_table .= '<tr>
            <td>
                Bitcoin dominance<br>
                <span style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round($metrics['btc_dom_percent_change_24h'], 2)) . '%</span><br>
                ' . str_replace('.', ',', round($metrics['btc_dom'], 2)) . '%
            </td>';
        if ($metrics['eth_dom_percent_change_24h'] >= 0) {
            $color = 'green';
            $sign = '+';
        } else {
            $color = 'red';
            $sign = '';
        }
        $html_metrics_table .= '<td>
                Ethereum dominance<br>
                <span style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round($metrics['eth_dom_percent_change_24h'], 2)) . '%</span><br>
                ' . str_replace('.', ',', round($metrics['eth_dom'], 2)) . '%
            </td>
        </tr>';

        if ($metrics['total_volume_percent_change_24h'] >= 0) {
            $color = 'green';
            $sign = '+';
                } else {
            $color = 'red';
            $sign = '';   
        }
        $html_metrics_table .= '<tr>
            <td>
                Volume 24h spot<br>
                <span style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round($metrics['total_volume_percent_change_24h'], 2)) . '%</span><br>
                <span class="rate_usd">$' . str_replace('.', ',', round($metrics['total_volume_24h'] * 0.000000001, 2)) . ' Mrds</span>
                <span class="rate_eur">' . str_replace('.', ',', round(($metrics['total_volume_24h'] / $rate_usd_eur) * 0.000000001, 2)) . '€ Mrds</span>
            </td>';
        if ($metrics['derivatives_percent_change_24h'] >= 0) {
            $color = 'green';
            $sign = '+';
        } else {
            $color = 'red';
            $sign = '';
        }
        $html_metrics_table .= '<td>
                Volume 24h dérivés<br>
                <span style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round($metrics['derivatives_percent_change_24h'], 2)) . '%</span><br>
                <span class="rate_usd">$' . str_replace('.', ',', round($metrics['derivatives_volume_24h'] * 0.000000001, 2)) . ' Mrds</span>
                <span class="rate_eur">' . str_replace('.', ',', round(($metrics['derivatives_volume_24h'] / $rate_usd_eur) * 0.000000001, 2)) . '€ Mrds</span>
            </td>
        </tr>';

        $html_metrics_table .= '</tbody></table>';

        return $html_metrics_table;
    }

}

?>