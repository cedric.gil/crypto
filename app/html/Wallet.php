<?php

namespace html;

class Wallet
{

    /**
     * Properties
     */

    private static $_instance;

    private $wallet;

    private $db;

    private $api_cmc;



    /**
     * Methods
     */

    public static function getInstance (\Wallet $wallet, \Api_CMC $api_cmc, \Database $db): Wallet
    {
        if (is_null(self::$_instance)) {
            self::$_instance = New Wallet($wallet, $api_cmc, $db);
        }
        return self::$_instance;
    }

    private function __construct (\Wallet $wallet, \Api_CMC $api_cmc, \Database $db)
    {
        $this->wallet = $wallet;
        $this->db = $db;
        $this->api_cmc = $api_cmc;
    }

    public function getGlobal (string $user, string $wallet_name): string
    {
        $wallet = $this->wallet->getWallet($wallet_name, $user);
        $prices = $this->api_cmc->getCryptoPrices($wallet);
        $rate_usd_eur = $this->api_cmc->getRate();

        $global_value = 0;
        $global_value_change = 0;
        $global_percent_change = 0;
        if (sizeof($wallet) > 0) {
            foreach ($wallet as $key => $value) {
                $global_value += $value * $prices[$key]['price'];
                $global_value_change += ($value * $prices[$key]['price']) - (($value * $prices[$key]['price']) / (($prices[$key]['percent_change_24h'] * 0.01) + 1));
            }
            $global_percent_change = (100 * $global_value_change) / ($global_value - $global_value_change);
        }


        $html = '<table>
            <tr>
                <th>' .  $wallet_name . '</th>
            </tr>
            <tr>
                <td>Valeur<br>';

        if ($global_percent_change >= 0) {
            $color = 'green';
            $sign = '+';
        } else {
            $color = 'red';
            $sign = '';
        }
        $html .= '<span style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round($global_percent_change, 2)) . '%</span>&emsp;';

        if ($global_value_change >= 0) {
            $color = 'green';
            $sign = '+';
        } else {
            $color = 'red';
            $sign = '-';
        }
        if (abs($global_value_change) < 1000) {
            $html .= '<span class="rate_usd" style="color: ' . $color . ';">' . $sign . '$' . str_replace('.', ',', round(abs($global_value_change), 2)) . '</span>
                <span class="rate_eur" style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round(abs($global_value_change / $rate_usd_eur), 2)) . '€</span><br>';
        } else {
            $html .= '<span style="color: ' . $color . ';">' . $sign . '$' . str_replace('.', ',', round(abs($global_value_change))) . '</span><br>';
        }

        if ($global_value < 1000) {
            $html .= '<span class="rate_usd">$' . str_replace('.', ',', round($global_value, 2)) . '</span>
                <span class="rate_eur">' . str_replace('.', ',', round($global_value / $rate_usd_eur, 2)) . '€</span>';
        } else {
            $html .= '<span class="rate_usd">$' . round($global_value) . '</span>
                <span class="rate_eur">' . round($global_value / $rate_usd_eur) . '€</span>';
        }

        $html .= '</td>
            </tr>
        </table>';

        return $html;
    }

    public function getWallet (string $user, string $wallet_name): string
    {
        $html = '<form method="post" id="edit_cryptos" action="/?p=me&sp=edit_wallet&wallet=' . $wallet_name . '">
        <table>
            <tbody>
                <tr>
                    <th>Actif</th>
                    <th>Quantité</th>
                    <th>Prix</th>
                    <th>Valeur</th>
                    <th>Dominance</th>
                </tr>';

        $brut_wallet = $this->wallet->getWallet($wallet_name, $user);
        $prices = $this->api_cmc->getCryptoPrices($brut_wallet);
        $wallet = $this->wallet->orderDescWallet($brut_wallet, $prices);
        $rate_usd_eur = $this->api_cmc->getRate();

        foreach ($wallet as $key => $value) {
            if ($value > 0) {
                $html .= '<tr>
                <td>' . $key . '</td>
                <td>
                    <input form="edit_cryptos" name="' . $key . '" value="' . $value . '" class="inputEditCrypto">
                    <div class="textCrypto">' . str_replace('.', ',', $value) . '</div>
                </td>';

                if ($prices[$key]['percent_change_24h'] >= 0) {
                    $color = 'green';
                    $sign = '+';
                } else {
                    $color = 'red';
                    $sign = '';
                }
                if ($prices[$key]['price'] >= 1000) {
                    $html .= '<td><span style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round($prices[$key]['percent_change_24h'], 2)) . '%</span><br>
                        <span class="rate_usd">$' . str_replace('.', ',', round($prices[$key]['price'])) . '</span>
                        <span class="rate_eur">' . str_replace('.', ',', round($prices[$key]['price'] / $rate_usd_eur)) . '€</span></td>';
                } elseif ($prices[$key]['price'] >= 1) {
                    $html .= '<td><span style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round($prices[$key]['percent_change_24h'], 2)) . '%</span><br>
                        <span class="rate_usd">$' . str_replace('.', ',', round($prices[$key]['price'], 2)) . '</span>
                        <span class="rate_eur">' . str_replace('.', ',', round($prices[$key]['price'] / $rate_usd_eur, 2)) . '€</span></td>';
                } elseif ($prices[$key]['price'] >= 0.0001) {
                    $html .= '<td><span style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round($prices[$key]['percent_change_24h'], 2)) . '%</span><br>
                        <span class="rate_usd">$' . str_replace('.', ',', round($prices[$key]['price'], 4)) . '</span>
                        <span class="rate_eur">' . str_replace('.', ',', round($prices[$key]['price'] / $rate_usd_eur, 4)) . '€</span></td>';
                } else {
                    $html .= '<td><span style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round($prices[$key]['percent_change_24h'], 2)) . '%</span><br>
                        <span class="rate_usd"><$0,0001</span>
                        <span class="rate_eur"><0,0001€</span></td>';
                }

                $value_change_24h = ($value * $prices[$key]['price']) - (($value * $prices[$key]['price']) / (($prices[$key]['percent_change_24h'] * 0.01) + 1));
                if ($value_change_24h >= 0) {
                    $color = 'green';
                    $sign = '+';
                } else {
                    $color = 'red';
                    $sign = '-';
                }
                if (abs($value_change_24h) < 1000) {
                    $html .= '<td><span class="rate_usd" style="color: ' . $color . ';">' . $sign . '$' . str_replace('.', ',', round(abs($value_change_24h), 2)) . '</span>
                        <span class="rate_eur" style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round(abs($value_change_24h / $rate_usd_eur), 2)) . '€</span><br>';
                } else {
                    $html .= '<td><span class="rate_usd" style="color: ' . $color . ';">' . $sign . '$' . str_replace('.', ',', round(abs($value_change_24h))) . '</span>
                        <span class="rate_eur" style="color: ' . $color . ';">' . $sign . str_replace('.', ',', round(abs($value_change_24h / $rate_usd_eur))) . '€</span><br>';
                }
                if (($prices[$key]['price'] * $value) < 1000) {
                    $html .= '<span class="rate_usd">$' . str_replace('.', ',', round($value * $prices[$key]['price'], 2)) . '</span>
                        <span class="rate_eur">' . str_replace('.', ',', round(($value * $prices[$key]['price']) / $rate_usd_eur, 2)) . '€</span></td>';
                } else {
                    $html .= '<span class="rate_usd">$' . str_replace('.', ',', round($value * $prices[$key]['price'])) . '</span>
                        <span class="rate_eur">' . str_replace('.', ',', round(($value * $prices[$key]['price']) / $rate_usd_eur)) . '€</span></td>';
                }

                if ($prices[$key]['market_cap_dominance'] >= 0.01) {
                    $html .= '<td>' . str_replace('.', ',', round($prices[$key]['market_cap_dominance'], 2)) . '%</td>';
                } else {
                    $html .= '<td><0,01%</td>';
                }

                $html .= '</tr>';
            }
        }

        $html .= '</tbody>
            </table>
        </form>';

        return $html;
    }

    public function getCryptosDatalist (): string
    {
        $datalist = '<datalist id="cryptos">';
        foreach ($this->db->select('SELECT crypto FROM cryptos') as $crypto) {
            $datalist .= '<option value="' . $crypto['crypto'] . '">';
        }
        $datalist .= '</datalist>';
        return $datalist;
    }

}

?>