window.addEventListener('load', function () {
    start()
})

function start () {
    openPopupAddCrypto()
    editWallet()
    changeRate()
}

function openPopupAddCrypto () {
    var btnAddCrypto = document.getElementById('button_add_crypto')
    var overlayPopupAddCrypto = document.getElementById('overlay_popup_add_crypto')
    var popupAddCrypto = document.getElementById('popup_add_crypto')
    var popupAddCryptotClicked = false
    var body = document.getElementsByTagName('body')[0]

    btnAddCrypto.addEventListener('click', function () {
        overlayPopupAddCrypto.style.display = 'flex'
        body.style.overflow = 'hidden'
    })

    popupAddCrypto.addEventListener('click', function () {
        popupAddCryptotClicked = true
    })
    overlayPopupAddCrypto.addEventListener('click', function () {
        if (popupAddCryptotClicked === false) {
            overlayPopupAddCrypto.style.display = 'none'
            body.style.overflow = 'auto'
        } else {
            popupAddCryptotClicked = false
        }
    })
}

function editWallet () {
    var btnEditWallet = document.getElementById('button_edit_wallet')
    var inputsEditCrypto = document.getElementsByClassName('inputEditCrypto')
    var textsCrypto = document.getElementsByClassName('textCrypto')
    var btnEditOk = document.getElementById('button_edit_ok')
    var inputDisplayed = false

    btnEditWallet.addEventListener('click', function () {
        if (inputDisplayed === false) {
            for (let i = 0; i < inputsEditCrypto.length; i++) {
                inputsEditCrypto[i].style.display = 'block'
            }
            for (let i = 0; i < textsCrypto.length; i++) {
                textsCrypto[i].style.display = 'none'
            }
            btnEditOk.style.display = 'block'
            inputDisplayed = true
        } else {
            for (let i = 0; i < inputsEditCrypto.length; i++) {
                inputsEditCrypto[i].style.display = 'none'
            }
            for (let i = 0; i < textsCrypto.length; i++) {
                textsCrypto[i].style.display = 'block'
            }
            btnEditOk.style.display = 'none'
            inputDisplayed = false
        }
    })
}

function changeRate () {
    var ratesEur = document.getElementsByClassName('rate_eur')
    var ratesUsd = document.getElementsByClassName('rate_usd')
    var btnChangeRate = document.getElementById('change_rate')
    var currentRate = 'USD'

    btnChangeRate.addEventListener('click', function () {
        if (currentRate === 'USD') {
            for (var i = 0; i < ratesEur.length; i++) {
                ratesUsd[i].style.display = 'none'
                ratesEur[i].style.display = 'inline'
            }
            btnChangeRate.innerHTML = '€/$'
            currentRate = 'EUR'
        } else if (currentRate === 'EUR') {
            for (var i = 0; i < ratesEur.length; i++) {
                ratesUsd[i].style.display = 'inline'
                ratesEur[i].style.display = 'none'
            }
            btnChangeRate.innerHTML = '$/€'
            currentRate = 'USD'
        }
    })
}