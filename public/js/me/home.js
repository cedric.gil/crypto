window.addEventListener('load', function () {
    start()
})

function start () {
    openPopupPlusWallet()
    deleteWallet()
    changeRate()
}

function openPopupPlusWallet () {
    var btnPlusWallet = document.getElementById('button_plus_wallet')
    var overlayPopupPlusWallet = document.getElementById('overlay_popup_plus_wallet')
    var popupPlusWallet = document.getElementById('popup_plus_wallet')
    var popupPlusWalletClicked = false
    var body = document.getElementsByTagName('body')[0]

    btnPlusWallet.addEventListener('click', function () {
        overlayPopupPlusWallet.style.display = 'flex'
        body.style.overflow = 'hidden'
    })

    popupPlusWallet.addEventListener('click', function () {
        popupPlusWalletClicked = true
    })
    overlayPopupPlusWallet.addEventListener('click', function () {
        if (popupPlusWalletClicked === false) {
            overlayPopupPlusWallet.style.display = 'none'
            body.style.overflow = 'auto'
        } else {
            popupPlusWalletClicked = false
        }
    })
}

function deleteWallet () {
    var btnLessWallet = document.getElementById('button_less_wallet')
    var divImgsDeleteWallet = document.getElementsByClassName('div_img_delete_wallet')
    var imgsDisplayed = false

    btnLessWallet.addEventListener('click', function () {
        if (imgsDisplayed) {
            for (var i = 0; i < divImgsDeleteWallet.length; i++) {
                divImgsDeleteWallet[i].style.display = 'none'
                imgsDisplayed = false
            }
        } else {
            for (var i = 0; i < divImgsDeleteWallet.length; i++) {
                divImgsDeleteWallet[i].style.display = 'flex'
                imgsDisplayed = true
            }
        }
    })
}

function changeRate () {
    var ratesEur = document.getElementsByClassName('rate_eur')
    var ratesUsd = document.getElementsByClassName('rate_usd')
    var rates_block = document.getElementById('top_10').children[1].children[0].children
    var btnChangeRate = document.getElementById('change_rate')
    var currentRate = 'USD'

    btnChangeRate.addEventListener('click', function () {
        if (currentRate === 'USD') {
            for (var i = 0; i < ratesEur.length; i++) {
                ratesUsd[i].style.display = 'none'
                ratesEur[i].style.display = 'inline'
            }
            for (var i = 0; i < rates_block.length; i++) {
                rates_block[i].children[3].style.display = 'none'
                rates_block[i].children[4].style.display = 'block'
            }
            btnChangeRate.innerHTML = '€/$'
            currentRate = 'EUR'
        } else if (currentRate === 'EUR') {
            for (var i = 0; i < ratesEur.length; i++) {
                ratesUsd[i].style.display = 'inline'
                ratesEur[i].style.display = 'none'
            }
            for (var i = 0; i < rates_block.length; i++) {
                rates_block[i].children[3].style.display = 'block'
                rates_block[i].children[4].style.display = 'none'
            }
            btnChangeRate.innerHTML = '$/€'
            currentRate = 'USD'
        }
    })
}